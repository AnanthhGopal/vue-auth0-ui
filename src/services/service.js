let request = require("request");
const API_URL = 'http://127.0.0.1:3651/';
import moment from "moment";
export default {
    async refershToken() {
        let token = '';
        let localStorageToken = localStorage.getItem('tokenForOauth');
        if (localStorageToken) {
            localStorageToken = JSON.parse(localStorageToken);
        }
        if (localStorageToken && localStorageToken['expireDateTime'] && localStorageToken['expires_in']) {
            let expiresIn = Number(localStorageToken['expires_in'])
            let expireTime = moment
                .utc(localStorageToken['expireDateTime'])
                .add((expiresIn - 2000), "seconds")
                .format();
            let currentTime = moment.utc(new Date()).format();
            if (expireTime && expireTime <= currentTime) {
                let tokenFromService = await this.getToken();
                if (tokenFromService && tokenFromService['access_token']) {
                    tokenFromService['expireDateTime'] = moment.utc(new Date()).format();
                    localStorage.setItem('tokenForOauth', JSON.stringify(tokenFromService));
                    token = tokenFromService['access_token'];
                }
            } else {
                token = localStorageToken['access_token'];
            }
        } else {
            let tokenFromService = await this.getToken();
            if (tokenFromService && tokenFromService['access_token']) {
                tokenFromService['expireDateTime'] = moment.utc(new Date()).format();
                localStorage.setItem('tokenForOauth', JSON.stringify(tokenFromService));
                token = tokenFromService['access_token'];
            }
        }
        return token;
    },
    getToken() {
        var options = {
            method: 'GET',
            url: API_URL + 'items/token?userId=1'
        };
        return new Promise(function (resolve, reject) {
            request(options, function (error, response, body) {
                if (error) throw new Error(error);
                try {
                    resolve(JSON.parse(body));
                } catch (e) {
                    reject(e);
                }
            })
        });
    },
    async getFoodItem() {
        let token = await this.refershToken();
        var options = {
            method: 'GET',
            url: API_URL + 'items',
            headers:
            {
                authorization: ('Bearer ' + token),
                'userid': '1'
            }
        };
        return new Promise(function (resolve, reject) {
            request(options, function (error, response, body) {
                if (error) throw new Error(error);
                try {
                    resolve(JSON.parse(body));
                } catch (e) {
                    reject(e);
                }
            })
        });
    },

    async getToken1(userId) {
        var options = {
            method: 'GET',
            url: API_URL + 'items/token?userId=' + userId;
        };
        return new Promise(function (resolve, reject) {
            request(options, function (error, response, body) {
                if (error) throw new Error(error);
                try {
                    resolve(JSON.parse(body));
                } catch (e) {
                    reject(e);
                }
            })
        });
    },
    getLocalStorageToken(userId) {
        let tokenFromLocalStorage = localStorage.getItem('authToken');
        if (tokenFromLocalStorage) {
            const parseToken = JSON.parse(tokenFromLocalStorage);
            if (parseToken && parseToken['token']) {
                return parseToken['token']
            } else {
                let tokenFromService = await this.getToken1(userId);
                localStorage.setItem('authToken', JSON.stringify({
                    token: tokenFromService
                }));
                return tokenFromService;
            }
        } else {
            let tokenFromService = await this.getToken1(userId);
            localStorage.setItem('authToken', JSON.stringify({
                token: tokenFromService
            }));
            return tokenFromService;
        }
    },
    async getFoodItem1() {
        let userId = 1;
        let token = await this.getLocalStorageToken(userId);
        var options = {
            method: 'GET',
            url: API_URL + 'items',
            headers:
            {
                authorization: ('Bearer ' + token),
                'userid': userId
            }
        };
        return new Promise(function (resolve, reject) {
            request(options, function (error, response, body) {
                if (error) throw new Error(error);
                try {
                    resolve(JSON.parse(body));
                } catch (e) {
                    reject(e);
                }
            })
        });
    }

}